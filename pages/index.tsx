
import Link from "next/link";
import { FC, ReactElement, ReactNode } from 'react';
// Components
import MainLayout from '../components/layouts/MainLayout'
import { useAppContext } from '../contexts/AppContext';

type Page = FC & { 
  getLayout?: (page: ReactElement) => ReactNode
}

const Home: Page = () => {
  const state = useAppContext();

  return (
    <>
      <h1>Welcome to {state.title}</h1>
      <div>
        <Link href="/about">
          <a style={{ margin: 5 }}>About</a>
        </Link>
        <Link href="/contact">
          <a style={{ margin: 5 }}>Contact</a>
        </Link>
      </div>
    </>
  )
}

Home.getLayout = function getLayout(page: ReactElement) {
  return (
    <MainLayout>
      {page}
    </MainLayout>
  )
}

export default Home
